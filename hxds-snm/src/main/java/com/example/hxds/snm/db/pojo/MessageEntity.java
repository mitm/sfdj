package com.example.hxds.snm.db.pojo;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;

/**
 * 发件人信息
 */
@Data
//Mongo注解，找到保存此实体类数据的集合（表）
@Document(collection = "message")
public class MessageEntity implements Serializable {
    @Id
    private String _id;

    @Indexed(unique = true)
    private String uuid;

    @Indexed
    private Long senderId;

    private String senderIdentity;

    private String senderPhoto = "https://hxds-public-1319433476.cos.ap-beijing.myqcloud.com/hxds/System.jpg";

    private String senderName;

    private String msg;

    @Indexed
    private Date sendTime;
}
