package com.example.hxds.mps.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public interface DriverLocationService {

    //更新位置缓存
    public void updateLocationCache(Map param);

    //删除位置缓存
    public void removeLocationCache(long driverId);

    //查找附近适合接单的司机
    public ArrayList searchBefittingDriverAboutOrder(double startPlaceLatitude,
                                                     double startPlaceLongitude,
                                                     double endPlaceLatitude,
                                                     double endPlaceLongitude,
                                                     double mileage);
    //用于司乘同显的定位信息，不能和上面的用一个，因为上面的是用在geo上面的
    public void updateOrderLocationCache(Map param);

    public HashMap searchOrderLocationCache(long orderId);
}
