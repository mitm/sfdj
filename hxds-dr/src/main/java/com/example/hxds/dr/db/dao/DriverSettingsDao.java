package com.example.hxds.dr.db.dao;

import com.example.hxds.dr.db.pojo.DriverSettingsEntity;

import java.util.Map;

/**
 * @Entity com.example.hxdsdr.db.pojo.DriverSettingsEntity
 */
public interface DriverSettingsDao {

    //添加司机设定记录
    public int insertDriverSettings(DriverSettingsEntity entity);

    public String searchDriverSettings(long driverId);
}




